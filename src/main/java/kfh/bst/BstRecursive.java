package kfh.bst;

import kfh.bst.interfaces.IBinarySearchTree;
import kfh.bst.interfaces.INode;

// Definitely NOT thread safe
public class BstRecursive extends BinarySearchTreeBase implements IBinarySearchTree {


	
	@Override
	public INode get(int key) {
        ChildParentNode cpn = get(key, getRoot(), null, false);
		if (cpn == null) {
			return null;
		}
		return cpn.getChildNode();
	}

	@Override
	public INode insert(int key) throws IllegalStateException {
		INode nodeToInsert = new Node(key);	
		INode rootNode = getRoot();
		if (rootNode == null) {
			setRoot(nodeToInsert);
		}
		else {
			insert(nodeToInsert, rootNode);
		}
		// If we got this far, no exceptions were thrown, so we can assume the insert succeeded
		incrementCount();
		return nodeToInsert;
	}

	@Override
	public boolean delete(int key) {
        ChildParentNode cpn = get(key, getRoot(), null, false);
		boolean deleted = delete(cpn);
		if (deleted) {
			decrementCount();
		}
		return deleted;
	}

	private int[] orderedKeys;
	private int orderedKeysIndex = 0;
	@Override
	public int[] getSorted() {
		orderedKeys = new int[getCount()];
		traverseInOrder(getRoot());
		return orderedKeys;
	}
	
	@Override
	public boolean validate() {
		return isBST(getRoot(), Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	
	/*    BEGIN - Private Helper Methods */
	
	
	private ChildParentNode get(int key, INode nodeToCheck, INode parentNode, boolean isLeftChild) {

		if (nodeToCheck == null) {
			return null;
		}
		
		int comparison = Integer.compare(key, nodeToCheck.getKey());
		if (comparison == 0) {

			return new ChildParentNode(nodeToCheck, parentNode, isLeftChild);
		}
		else if (comparison < 0) {
			return get(key, nodeToCheck.getLeft(), nodeToCheck, true);
		}
		// comparison > 0
		return get(key, nodeToCheck.getRight(), nodeToCheck, false);
		
	}
	
	private void insert(INode nodeToInsert, INode parentNode) {
		if (nodeToInsert == null || parentNode == null) {
			throw new IllegalStateException("nodeToInsert and parentNode must not be null");
		}
		
		int comparison = Integer.compare(nodeToInsert.getKey(), parentNode.getKey());
		if (comparison == 0) {
			String errorMsg = String.format("A node with key=%1s already exists", nodeToInsert.getKey());
			throw new IllegalStateException(errorMsg);
		}
		else if (comparison < 0) {
			INode leftNode = parentNode.getLeft();
			if (leftNode != null) {
				insert(nodeToInsert, leftNode);
			}
			else {
				parentNode.setLeft(nodeToInsert);
			}
		}
		else { // comparison > 0
			INode rightNode = parentNode.getRight();
			if (rightNode != null) {
				insert(nodeToInsert, rightNode);
			}
			else {
				parentNode.setRight(nodeToInsert);
			}			
		}
		
	}
		
	private boolean delete(ChildParentNode childParentNode) {
		if (childParentNode == null) {
			return false;
		}
		int childCount = getNodeChildCount(childParentNode.getChildNode());
		switch (childCount) {
		case 0:
			return deleteNodeWithoutChildren(childParentNode);
		case 1:
			return deleteNodeWithOneChild(childParentNode);
		default:
			return deleteNodeWithTwoChildren(childParentNode);
		}

	}
	
	private boolean deleteNodeWithoutChildren(ChildParentNode childParentNode) {
		// if the parent is null, we are deleting the root. since there are no children, just set the root to null
		if (childParentNode.getParentNode() == null) {
			setRoot(null); 
		}
		else if (childParentNode.getIsLeftChild()) {
			childParentNode.getParentNode().setLeft(null);
		}
		else { // right child
			childParentNode.getParentNode().setRight(null);
		}
		return true;		
	}
	
	private boolean deleteNodeWithOneChild(ChildParentNode childParentNode) {
		
		INode replacementNode = childParentNode.getChildNode().getLeft() != null ? 
				childParentNode.getChildNode().getLeft() :
				childParentNode.getChildNode().getRight();
		
		// if the parent is null, we are deleting the root. Just set the root to the singlr child
		if (childParentNode.getParentNode() == null) {
			setRoot(replacementNode); 
		}
		else if (childParentNode.getIsLeftChild()) {
			childParentNode.getParentNode().setLeft(replacementNode);
		}
		else { // right child
			childParentNode.getParentNode().setRight(replacementNode);
		}
		return true;		
	}
	
	private boolean deleteNodeWithTwoChildren(ChildParentNode childParentNode) {
		
		// 1) find the in-order successor of the node to delete 
		// 2) replace the key of the node to delete with the key of the in-order successor
		// 3) delete the in-order successor

		INode nodeToDelete = childParentNode.getChildNode();
		ChildParentNode replacementNode = getInOrderSuccessor(nodeToDelete);
		nodeToDelete.setKey(replacementNode.getChildNode().getKey());
		return delete(replacementNode);
		
	}
	
	
	private int getNodeChildCount(INode node) {
		int childCount = 0;
		if (node.getLeft() != null) {
			childCount++;
		}
		if (node.getRight() != null) {
			childCount++;
		}
		return childCount;
	}
	
	
	private void traverseInOrder(INode startNode) {
		if (startNode == null) {
			return;
		}
		
		// Walk down the left
		traverseInOrder(startNode.getLeft());
		
		// Add the current node's key to the orderedKeys
		orderedKeys[orderedKeysIndex] = startNode.getKey();
		orderedKeysIndex++;
		
		// Walk down the Right
		traverseInOrder(startNode.getRight());			
	}
	
	private ChildParentNode getInOrderSuccessor(INode node) {
		// in-order successor is the left most node of the node's right child
		return getLeftMostChild(node.getRight(), null);
	}
	
	private ChildParentNode getLeftMostChild(INode node, INode parent) {
		if (node == null) {
			return null;
		}
		if (node.getLeft() != null) {
			return getLeftMostChild(node.getLeft(), node);
		}
		return new ChildParentNode(node, parent, true);
	}
	
	// c++ code copied directly from Wikipedia and translated to Java - https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
	private boolean isBST(INode node, int minKey, int maxKey) {
	    if(node == null)  {
	    	return true;
	    }
	    
	    if(node.getKey() < minKey || node.getKey() > maxKey) return false;
	    
	    return isBST(node.getLeft(), minKey, node.getKey()) && isBST(node.getRight(), node.getKey(), maxKey);
	}	
		
	private class ChildParentNode {
		
		private INode _childNode;
		private INode _parentNode;
		private boolean _isLeftChild;
		
		public ChildParentNode(INode childNode, INode parentNode, boolean isLeftChild) {
			_childNode = childNode;
			_parentNode = parentNode;
			_isLeftChild = isLeftChild;
		}
		
		INode getChildNode() {
			return _childNode;
		}

		INode getParentNode() {
			return _parentNode;
		}
		
		boolean getIsLeftChild() {
			return _isLeftChild;
		}
		
	}
	
	/*    END - Private Helper Methods */	
	

}
