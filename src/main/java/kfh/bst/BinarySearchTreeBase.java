package kfh.bst;

import kfh.bst.interfaces.IBinarySearchTree;
import kfh.bst.interfaces.INode;


public abstract class BinarySearchTreeBase implements IBinarySearchTree {

	private INode _rootNode = null;
	private int _count;
	
	public final INode getRoot() {
		return _rootNode;
	}

	protected void setRoot(INode rootNode) {
		_rootNode = rootNode;
	}
	
	public abstract INode get(int key);

	protected synchronized void incrementCount() {
		 _count++;
	}
	
	protected synchronized void decrementCount() {
		_count--;
	}
	
	public final int getCount() {
		return _count;
	}
	
	public abstract INode insert(int key) throws IllegalStateException;

	public abstract boolean delete(int key);

	public abstract int[] getSorted();
	
	public abstract boolean validate();

}
