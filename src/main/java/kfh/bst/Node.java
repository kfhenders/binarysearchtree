package kfh.bst;

import kfh.bst.interfaces.INode;

public class Node implements INode {

	private int _key;
	private INode _left;
	private INode _right;
	
	public Node() { 
		this(Integer.MIN_VALUE);
	}

	public Node(int key) {
		_key = key;
		_left = null;
		_right = null;
	}
	
	public int getKey() {
		return _key;
	}

	public void setKey(int key) {
		_key = key;
	}

	public INode getLeft() {
		return _left;
	}

	public void setLeft(INode node) {
		_left = node;
	}

	public INode getRight() {
		return _right;
	}

	public void setRight(INode node) {
		_right = node;	}
	
}
