package kfh.bst.interfaces;

public interface IBinarySearchTree {
	
	INode getRoot();
	INode get(int key);
	int getCount();
	INode insert(int key) throws IllegalStateException;
	boolean delete(int key);
	int[] getSorted();
	boolean validate();
}
