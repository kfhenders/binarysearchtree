package kfh.bst.interfaces;

public interface INode {
	int getKey();
	void setKey(int key);
	INode getLeft();
	void setLeft(INode node);
	INode getRight();
	void setRight(INode node);
}
