package kfh.bst;

import static org.junit.Assert.*;

import org.junit.Test;

import kfh.bst.interfaces.INode;

public class NodeTests {

	@Test
	public void constructor_keySpecified_setsKey() {
		int expectedKey = 12;
		INode node = new Node(expectedKey);
		int actualKey = node.getKey();
		assertEquals(expectedKey, actualKey);		
	}
	
	@Test
	public void getKey_beforeSet_returnsMinValue() {
		int expectedKey = Integer.MIN_VALUE;
		INode node = new Node();
		int actualKey = node.getKey();
		assertEquals(expectedKey, actualKey);
	}
	
	@Test
	public void setGetKey_afterSet_getReturns() {
		int expectedKey = 12;
		INode node = new Node();
		node.setKey(expectedKey);
		int actualKey = node.getKey();
		assertEquals(expectedKey, actualKey);
	}

	@Test
	public void getLeft_beforeSet_returnsNull() {
		INode node = new Node();
		INode left = node.getLeft();
		assertNull(left);
	}

	@Test
	public void setGetLeft_afterSet_getReturns() {
		INode parentNode = new Node();
		INode expectedChild = new Node();
		parentNode.setLeft(expectedChild);
		INode actualChild = parentNode.getLeft();
		assertEquals(expectedChild, actualChild);
	}

	@Test
	public void setLeft_beforeSet_returnsNull() {
		INode node = new Node();
		INode right = node.getRight();
		assertNull(right);
	}
	
	@Test
	public void setGetRight_afterSet_getReturns() {
		INode parentNode = new Node();
		INode expectedChild = new Node();
		parentNode.setRight(expectedChild);
		INode actualChild = parentNode.getRight();
		assertEquals(expectedChild, actualChild);	
	}

}
