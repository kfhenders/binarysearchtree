package kfh.bst;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import kfh.bst.interfaces.IBinarySearchTree;
import kfh.bst.interfaces.INode;
import org.apache.commons.lang3.ArrayUtils;
import  java.util.Arrays;

public abstract class BinarySearchTreeTestsBase<T extends IBinarySearchTree> {

	/*    
	 * 
	 *                                         The test tree                               
	 *                                            
	 *                                               11
	 *                                            /       \
	 *                                         6           19
	 *                                       /   \       /    \
	 *                                      4     8     17      43
	 *                                       \     \          /   \
	 *                                        5     10      31     49     
	 *                                             /       /      /  \
	 *                                            9*      29*   45*   55*
	 *                                      
	 *                                       * = Inserted after initialization  for some tests                                
	 */
	
	T _instance;
	int[] _keys = {11, 6, 8, 19, 4, 10, 5, 17, 43, 49, 31};
	int _rootKey = 11;
	int[] _row2 = {6, 19 };
	int[] _row3 = {4, 8, 17, 43};
	int[] _row4 = {5, 10, 31, 49};
	int[] _row5 = {29, 45, 9, 55 };
	
	abstract T createInstance();
	
	@Before
	public void setUp() throws Exception {
		_instance = createInstance();
		for (int i : _keys) {
			_instance.insert(i);
		}
	}

	@Test
	public void getCount_afterInitialize_returnsExpected() {
		int count = _instance.getCount();
		assertEquals(_keys.length, count);
	}
	
	@Test
    public void getCount_afterInsertRow5_countIncremented() {
		
		int expectedCount = _keys.length + _row5.length;
		
		insertRow5();
		int actualCount = _instance.getCount();
		
		assertEquals(expectedCount, actualCount);
		
	}
	
	@Test
	public void getCount_delete_countsDecremented() {
		int[] keysToDelete = { _keys[3], _keys[5], _keys[8] };
		int expectedCount = _keys.length;
		
		for (int i : keysToDelete) {
			_instance.delete(i);
			expectedCount--;
			int actualCount = _instance.getCount();
			assertEquals(expectedCount, actualCount);
		}
	}
	
	@Test
	public void getRoot_afterInitialize_returnsExpected() {
		INode root = _instance.getRoot();
		int rootKey = root.getKey();
		int rootLeftNodeKey = root.getLeft().getKey();
		int rootRightNodeKey = root.getRight().getKey();
		
		assertEquals(_rootKey, rootKey);
		assertEquals(_row2[0], rootLeftNodeKey);
		assertEquals(_row2[1], rootRightNodeKey);
	}

	@Test 
	public void getRoot_deleteRoot_setsNewRootAsExpected() {
		int currentRootKey = 11;
		int expectedNewRootKey = 17;
		
		_instance.delete(currentRootKey);
		int actualNewRootKey = _instance.getRoot().getKey();
		
		assertEquals(expectedNewRootKey, actualNewRootKey);
	}
	
	@Test
	public void get8_afterInitialize_returnsExpected() {
		
		int expectedNodeKey = 8;
		int expectedRightKey = 10;
		
		INode expectedNode = _instance.get(expectedNodeKey);
		int actualKey = expectedNode.getKey();
		INode actualLeftNode = expectedNode.getLeft();
		int actualRightKey = expectedNode.getRight().getKey();
		
		assertEquals(expectedNodeKey, actualKey);
		assertNull(actualLeftNode);
		assertEquals(expectedRightKey, actualRightKey);
	}
		
	@Test
	public void get43_afterInitialize_ReturnsExpected() {
		int expectedNodeKey = 43;
		int expectedLeftKey = 31;
		int expectedRightKey = 49;
		
		INode expectedNode = _instance.get(expectedNodeKey);
		int actualKey = expectedNode.getKey();
		int actualLeftNodeKey = expectedNode.getLeft().getKey();
		int actualRightKey = expectedNode.getRight().getKey();
		
		assertEquals(expectedNodeKey, actualKey);
		assertEquals(expectedLeftKey, actualLeftNodeKey);
		assertEquals(expectedRightKey, actualRightKey);
	}
	
	@Test
	public void get_invalidKey_returnsNull() {
		INode returnedNode = _instance.get(Integer.MAX_VALUE);
		assertNull(returnedNode);
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void insert_duplicateRootKey_throws() {
		_instance.insert(_keys[0]);
	}
	
	@Test(expected=IllegalStateException.class)
	public void insert_duplicateKey_throws() {
		_instance.insert(_keys[5]);
	}
	
	@Test
	public void getSorted_afterInitialize_ReturnsExpected() {
		int[] expectedArray = _keys.clone();
		Arrays.sort(expectedArray);
		
		int[] actualArray = _instance.getSorted();
		
		assertArrayEquals(expectedArray, actualArray);
	}
	
	@Test
	public void getSorted_afterInsertRow5_ReturnsExpected() {
		
		int[] expectedArray = ArrayUtils.addAll(_keys, _row5);
		Arrays.sort(expectedArray);
		
		insertRow5();
		int[] actualArray = _instance.getSorted();
		
		assertArrayEquals(expectedArray, actualArray);
	}

	@Test
	public void insert_afterInsertRow5_nodeWithKey10isCorrect() {
		
		int node10Key = 10;
		int expectedLeftNodeKey = 9;
		
		insertRow5();
		INode node10 = _instance.get(node10Key);
		int actualLeftNodeKey = node10.getLeft().getKey();
		INode rightNode = node10.getRight();
		
		assertEquals(expectedLeftNodeKey, actualLeftNodeKey);
		assertNull(rightNode);
	}
	
	@Test
	public void insert_afterInsertRow5_nodeWithKey31isCorrect() {
		
		int node31Key = 31;
		int expectedLeftNodeKey = 29;
		
		insertRow5();
		INode node31 = _instance.get(node31Key);
		int actualLeftNodeKey = node31.getLeft().getKey();
		INode rightNode = node31.getRight();
		
		assertEquals(expectedLeftNodeKey, actualLeftNodeKey);
		assertNull(rightNode);
	
	}	
	
	@Test
	public void insert_afterInsertRow5_nodeWithKey49isCorrect() {
		
		int node49Key = 49;
		int expectedLeftNodeKey = 45;
		int expectedRightNodeKey = 55;
		
		insertRow5();
		INode node49 = _instance.get(node49Key);
		int actualLeftNodeKey = node49.getLeft().getKey();
		int actualRightNodeKey = node49.getRight().getKey();
		
		assertEquals(expectedLeftNodeKey, actualLeftNodeKey);
		assertEquals(expectedRightNodeKey, actualRightNodeKey);
	}
	
	@Test
	public void delete_nodeWithNoChildren_justDeletesNode() {
		
		// 17 is the left node of 19 and has no children
		int keyToDelete = 17;
		int parentKey = 19;
		
		_instance.delete(keyToDelete);
		
		INode missingNode = _instance.get(keyToDelete);
		INode parentNode = _instance.get(parentKey);
				
		assertNull(missingNode);
		assertNull(parentNode.getLeft());
		
	}
	
	@Test
	public void delete_nodeWithLeftChildOnly_replacesSelfWithLeftChild() {
		
		// Insert a 9 which will be the left child of 10, then delete 10
		// 9 should move to become the right node of 10's parent which is 8 
		int leftChildKey = 9;
		int keyToDelete = 10;
		int parentKey = 8;
		
		_instance.insert(leftChildKey);
		_instance.delete(keyToDelete);
		
		INode deletedNode = _instance.get(keyToDelete);
		INode parentNode = _instance.get(parentKey);
		int parentRightNodeKey = parentNode.getRight().getKey();
		
		assertNull(deletedNode);
		assertEquals(leftChildKey, parentRightNodeKey);
		
	}
	
	@Test
	public void delete_nodeWithRightChildOnly_replacesSelfWithRightChild() {
		
        // Node 8 only has a right child with key=10
		// If we delete 8, 10 should become the right child of 8's parent, which is 6
		int keyToDelete = 8;
		int parentKey = 6;
		int expectedNewRightChildKey = 10;
		
		_instance.delete(keyToDelete);
		
		INode deletedNode = _instance.get(keyToDelete);
		INode parentNode = _instance.get(parentKey);
		int actualNewRightChildKey = parentNode.getRight().getKey();
		
		assertNull(deletedNode);
		assertEquals(expectedNewRightChildKey, actualNewRightChildKey);
		
	}
	
	// This test is a mess and needs to be refactored
	@Test
	public void delete_nodeWith2Children_replacesSelfWithInOrderSuccessor() {

		// Let's add Row 5, then insert 30 which will become the right child of 29
		// Then Delete 19 which should be replaced by 29 (it's in order successor)
		// 30 should then move to 29's previous position and 29 should move to
		// 19's previous position
		int keyToDelete = 19;
		INode currentKeyNode = _instance.get(keyToDelete);
		int currentKeyNodeLeftKey = currentKeyNode.getLeft().getKey();
		int currentKeyNodeRightKey = currentKeyNode.getRight().getKey();
		
		int expectedReplacementKey = 29;
        int expectedReplacementKeyParentKey = 31;
		int expectedReplacementKeyNewRightChildKey = 30;
        
        insertRow5();
        _instance.insert(expectedReplacementKeyNewRightChildKey);
        
        _instance.delete(keyToDelete);
        
        // Node 29 should now have the same left and right children that 19 had before
        INode replacementNode = _instance.get(expectedReplacementKey);
		int replacementNodeLeftKey = replacementNode.getLeft().getKey();
		assertEquals(currentKeyNodeLeftKey, replacementNodeLeftKey);
		int replacementNodeRightKey = replacementNode.getRight().getKey();
		assertEquals(currentKeyNodeRightKey, replacementNodeRightKey);
		
		// Node 31 should now have 30 as its left child
		INode actualReplacementKeyParentNode = _instance.get(expectedReplacementKeyParentKey);
		int actualNode30LeftKey  = actualReplacementKeyParentNode.getLeft().getKey();
		assertEquals(expectedReplacementKeyNewRightChildKey, actualNode30LeftKey);
	}
	
	@Test
	public void validate_afterInitialization_returnsTrue() {
		assertTrue(_instance.validate());
	}
	
	@Test
	public void validate_afterInsertRow5_returnsTrue() {
		insertRow5();
		assertTrue(_instance.validate());
	}	
	
	@Test
	public void validate_keyLessThanRootOnRight_returnsFalse() {
		// Insert a 3 as the Left child of node 17
		
		int keyToGet = 17;
		INode invalidNode = new Node(3);
		
		INode nodeToGet = _instance.get(keyToGet);
		nodeToGet.setLeft(invalidNode);
				
		assertFalse(_instance.validate());
	}	
	
	@Test
	public void validate_keyGreaterThanRootOnLeft_returnsFalse() {
		// Insert a 20 as the Right child of node 10
		
		int keyToGet = 10;
		INode invalidNode = new Node(20);
		
		INode nodeToGet = _instance.get(keyToGet);
		nodeToGet.setRight(invalidNode);
				
		assertFalse(_instance.validate());
	}	
	
	private void insertRow5() {
		for (int i : _row5) {
			_instance.insert(i);
		}
	}
}
