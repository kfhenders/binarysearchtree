package kfh.bst;

import static org.junit.Assert.*;
import org.junit.Test;

import kfh.bst.interfaces.INode;

public class BstRecursiveTests extends BinarySearchTreeTestsBase<BstRecursive> {

	@Override
	BstRecursive createInstance() {
		return new BstRecursive();
	}
	
   /* One off tests specific to BstRecursive implementation go here  */	
	
	@Test
	public void getRoot_afterInsert_returnsExpected() {
		
		BstRecursive bst = new BstRecursive();
		int expectedKey = 12;
		
		bst.insert(expectedKey);
		int actualKey = bst.getRoot().getKey();
		
		assertEquals(expectedKey, actualKey);
	}	
	
	@Test
	public void getRoot_afterInsertDelete_returnsNull() {
		
		BstRecursive bst = new BstRecursive();
		int rootKey = 12;
		
		bst.insert(rootKey);
		bst.delete(rootKey);
		INode rootNode = bst.getRoot();
		
		assertNull(rootNode);
	}		

}
